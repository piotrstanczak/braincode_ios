//
//  define.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#ifndef Braincode_define_h
#define Braincode_define_h

#define MAIN_RED_COLOR                              0xfa1625

#define MENU_COLOR                                  0x90278e
#define MENU_SELECTED_COLOR                         0xc33bc1

#define NAV_COLOR                                   0xdcdcdc

#endif
