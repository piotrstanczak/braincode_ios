//
//  JsonParser.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonParser : NSObject

+ (NSArray *)arrayWithJson:(id)json;
+ (NSDictionary *)dictionaryWithJson:(id)json;
+ (NSString *)stringWithJson:(id)json;
+ (NSNumber *)numberWithJson:(id)json;

@end
