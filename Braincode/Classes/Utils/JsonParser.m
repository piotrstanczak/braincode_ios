//
//  JsonParser.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "JsonParser.h"

@implementation JsonParser

+ (NSArray *)arrayWithJson:(id)json
{
    return [json isKindOfClass:[NSArray class]] ? [NSArray arrayWithArray:json] : nil;
}

+ (NSDictionary *)dictionaryWithJson:(id)json
{
    return [json isKindOfClass:[NSDictionary class]] ? [NSDictionary dictionaryWithDictionary:json] : nil;
}

+ (NSString *)stringWithJson:(id)json
{
//    return [json isKindOfClass:[NSString class]] ? (NSString *)json : nil;
    return [json isKindOfClass:[NSString class]] ? [NSString stringWithString:json] : nil;
}

+ (NSNumber *)numberWithJson:(id)json
{
    return [json isKindOfClass:[NSNumber class]] ? (NSNumber *)json : nil;
}


@end
