//
//  UINavigationController+FMA.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "UINavigationController+FMA.h"

@implementation UINavigationController (FMA)

- (UIViewController *)rootViewController
{
    return self.viewControllers.firstObject;
}

@end
