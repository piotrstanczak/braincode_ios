//
//  ContentManager.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "ContentManager.h"
#import "ApiManager.h"
#import "AllegroApiManager.h"
#import "CategoriesItem.h"
#import "OfferItem.h"
#import "JsonParser.h"

static ContentManager *sharedManager = nil;

@implementation ContentManager

- (id)init
{
    NSAssert(sharedManager == nil, @"Can be only one instance of ContentManager class.");
    
    self = [super init];
    
    if (self)
    {
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    @synchronized(self)
    {
        if (!sharedManager)
        {
            sharedManager = [ContentManager new];
        }
        
        return sharedManager;
    }
}

- (void)getCategoriesWithSuccess:(void (^)(NSArray *data))success
                             failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                                view:(UIView *)view
{
    [[AllegroApiManager sharedManager] getCategoriesWithSuccess:^(NSArray *data) {
        
        NSMutableArray *categoriesData = [NSMutableArray array];
        
        for (int i = 0; i < data.count; i++)
        {
            NSLog(@"data: %@", [data objectAtIndex:i]);
            
            id object = [data objectAtIndex:i];
            
            NSString *categoriesId = [JsonParser stringWithJson:object[@"id"]];
            
            NSString *title = [JsonParser stringWithJson:object[@"name"]];
            
            CategoriesItem *categoriesItem = [[CategoriesItem alloc] initWithCategoriesId:categoriesId andTitle:title];
            
            [categoriesData addObject:categoriesItem];
        }
        
        NSLog(@"data: %@", @(data.count));
        
        
        if (success) {
            success(categoriesData);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"categories fail: %@ %@", @(operation.response.statusCode), error.localizedDescription);
        
    }];
}

- (void)getOffersWithSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                            view:(UIView *)view
{
    
    [[ApiManager sharedManager] getOffersWithSuccess:^(NSArray *data) {
        
        NSLog(@"data: %@", @(data.count));
        
        NSMutableArray *offersData = [NSMutableArray array];
        
        for (int i = 0; i < data.count; i++)
        {
            id object = [data objectAtIndex:i];
            
            OfferItem *offerItem = [[OfferItem alloc] init];
            offerItem.offerId = [JsonParser stringWithJson:object[@"id"]];
            offerItem.imageUrl = @"http://www.owocowy.sklep.pl/galerie/k/koszulka-fitted-valuewei_376.jpg";
            
            NSString *imageString = [[[JsonParser stringWithJson:object[@"image"]] componentsSeparatedByString:@","] lastObject];
            NSData *imageData = [NSData dataWithBase64String:imageString];
            offerItem.imageData = imageData;
            
            offerItem.title = [JsonParser stringWithJson:object[@"title"]];
            offerItem.desc = [JsonParser stringWithJson:object[@"description"]];
            offerItem.sellerId = [JsonParser stringWithJson:object[@"sellerId"]];
            offerItem.price = [JsonParser numberWithJson:object[@"price"]];
            offerItem.regularPrice = [JsonParser numberWithJson:object[@"regularPrice"]];
            offerItem.endTime = [JsonParser stringWithJson:object[@"endTime"]];
            
            [offersData addObject:offerItem];
        }
        
        if (success) {
            success(offersData);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"categories fail: %@ %@", @(operation.response.statusCode), error.localizedDescription);
    }];
    
    
//    [[ApiManager sharedManager] getOffersWithSuccess:^(NSArray *data) {
//        
//        NSMutableArray *categoriesData = [NSMutableArray array];
//        
//        for (int i = 0; i < data.count; i++)
//        {
//            NSLog(@"data: %@", [data objectAtIndex:i]);
//            
//            id object = [data objectAtIndex:i];
//            
//            NSString *categoriesId = [JsonParser stringWithJson:object[@"id"]];
//            
//            NSString *title = [JsonParser stringWithJson:object[@"name"]];
//            
//            CategoriesItem *categoriesItem = [[CategoriesItem alloc] initWithCategoriesId:categoriesId andTitle:title];
//            
//            [categoriesData addObject:categoriesItem];
//        }
//        
//        NSLog(@"data: %@", @(data.count));
//        
//        
//        if (success) {
//            success(categoriesData);
//        }
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        NSLog(@"categories fail: %@ %@", @(operation.response.statusCode), error.localizedDescription);
//        
//    }];
}

@end
