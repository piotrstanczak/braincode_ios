//
//  MenuItem.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *menuId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL isSelected;

- (id)initWithMenuId:(NSString *)menuId andTitle:(NSString *)title;

@end
