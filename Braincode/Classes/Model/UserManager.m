//
//  UserManager.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "UserManager.h"
#import "AllegroApiManager.h"
#import "ApiManager.h"

#define kApiSessionKey         @"x-nifc-session"
#define keyToken               @"access_token"

static UserManager *sharedManager = nil;

@interface UserManager ()

@end

@implementation UserManager

- (id)init
{
    NSAssert(sharedManager == nil, @"Can be only one instance of UserManager class.");
    
    self = [super init];
    
    if (self)
    {
        _user = [[User alloc] init];

        [self addSessionHeaderWithSessionToken:_user.sessionToken];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    @synchronized(self)
    {
        if (!sharedManager)
        {
            sharedManager = [UserManager new];
        }
        
        return sharedManager;
    }
}

- (void)addSessionHeaderWithSessionToken:(NSString *)sessionToken
{
    [[[AllegroApiManager sharedManager] requestSerializer] setValue:sessionToken forHTTPHeaderField:kApiSessionKey];
}

- (BOOL)isLogged
{
    return _user ? YES : NO;
}

- (void)authenticationWithSuccess:(void (^)())success
                          failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
{
    [[AllegroApiManager sharedManager] authenticationWithSuccess:^() {
        
        if (success)
            success();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation.response.statusCode, error.localizedDescription);
        
    }];
}

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void (^)())success
                  failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                     view:(UIView *)view
{
    [[AllegroApiManager sharedManager] loginWithUsername:username password:password accessToken:nil success:^(NSString *userId) {
       
        NSLog(@"login: %@", userId);
        
        _user.username = username;
        _user.password = password;
        _user.identifier = userId;
        
        [[ApiManager sharedManager] syncUserWithUserId:_user.identifier userName:_user.username userPass:@"password" success:^{
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation.response.statusCode, error.localizedDescription);
    }];
}

@end
