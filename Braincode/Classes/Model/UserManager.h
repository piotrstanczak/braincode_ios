//
//  UserManager.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface UserManager : NSObject

@property (nonatomic, strong) User *user;

+ (instancetype)sharedManager;

- (BOOL)isLogged;

- (void)logoutUser;

- (void)authenticationWithSuccess:(void (^)())success
                          failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure;

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
                  success:(void (^)())success
                  failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                     view:(UIView *)view;

@end
