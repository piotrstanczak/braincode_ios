//
//  OfferItem.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "OfferItem.h"

@implementation OfferItem

-(NSString *)getDate
{
    NSDate *timestamp = [NSDate dateWithTimeIntervalSince1970:[self.endTime doubleValue]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"dd MMMM"];
    return [dateFormatter stringFromDate:timestamp];
}

@end
