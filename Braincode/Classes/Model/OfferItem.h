//
//  OfferItem.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferItem : NSObject

@property (nonatomic, strong) NSString *offerId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *sellerId;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *regularPrice;
@property (nonatomic, strong) NSNumber *amountToSell;
@property (nonatomic, strong) NSString *endTime;

- (NSString *)getDate;

//sellerId": 1,
//"price": 15,
//"regularPrice": 22,
//"amountToSell": 15,
//"endTime": 1426483355

@end
