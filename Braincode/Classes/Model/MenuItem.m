//
//  MenuItem.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

- (id)initWithMenuId:(NSString *)menuId andTitle:(NSString *)title
{
    self = [super init];
    
    if (self)
    {
        _menuId = menuId;
        _title = title;
    }
    
    return self;
}

@end
