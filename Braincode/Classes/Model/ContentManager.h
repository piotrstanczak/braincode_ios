//
//  ContentManager.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentManager : NSObject

+ (instancetype)sharedManager;

- (void)getCategoriesWithSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                            view:(UIView *)view;

- (void)getOffersWithSuccess:(void (^)(NSArray *data))success
                     failure:(void (^)(NSInteger errorCode, NSString *errorMessage))failure
                        view:(UIView *)view;
@end
