//
//  CategoriesItem.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "CategoriesItem.h"

@implementation CategoriesItem

- (id)initWithCategoriesId:(NSString *)categoriesId andTitle:(NSString *)title
{
    self = [super init];
    
    if (self)
    {
        _categoriesId = categoriesId;
        _title = title;
    }
    
    return self;
}

@end
