//
//  User.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *sessionToken;

@end
