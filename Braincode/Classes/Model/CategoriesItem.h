//
//  CategoriesItem.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoriesItem : NSObject

@property (nonatomic, strong) NSString *categoriesId;
@property (nonatomic, strong) NSString *title;

- (id)initWithCategoriesId:(NSString *)categoriesId andTitle:(NSString *)title;

@end
