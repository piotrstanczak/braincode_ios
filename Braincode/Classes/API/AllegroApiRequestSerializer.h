//
//  AllegroApiRequestSerializer.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "AFURLRequestSerialization.h"

@interface AllegroApiRequestSerializer : AFHTTPRequestSerializer

@end
