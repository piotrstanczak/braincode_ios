//
//  ApiManager.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "ApiManager.h"
#import "AllegroApiRequestSerializer.h"
#import "JsonParser.h"
#import "User.h"

static ApiManager *sharedManager = nil;

@implementation ApiManager

- (id)init
{
    NSAssert(sharedManager == nil, @"Can be only one instance of ApiManager class.");
    
    NSURL *url = [NSURL URLWithString:@"http://braincodemobile.azure-mobile.net/"];
    self = [super initWithBaseURL:url];
    
    if (self)
    {
        self.requestSerializer = [AllegroApiRequestSerializer new];
        self.responseSerializer = [AFJSONResponseSerializer new];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    @synchronized(self)
    {
        if (sharedManager == nil)
        {
            sharedManager = [[ApiManager alloc] init];
        }
    }
    return sharedManager;
}

- (void)getOffersWithSuccess:(void (^)(NSArray *data))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [super GET:@"GetOffers" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *dataArray = [JsonParser arrayWithJson:responseObject];
        
        if (dataArray && success)
            success(dataArray);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation, error);
    }];
}

- (void)syncUserWithUserId:(NSString*)userId
                  userName:(NSString*)userName
                  userPass:(NSString*)userPass
                   success:(void (^)())success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *path = [NSString stringWithFormat:@"User/%@/%@/%@", userId, userName, userPass];
    
    [super POST:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success)
            success();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation, error);
    }];
}

@end
