//
//  ApiManager.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface ApiManager : AFHTTPRequestOperationManager

+ (instancetype)sharedManager;

- (void)getOffersWithSuccess:(void (^)(NSArray *data))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)syncUserWithUserId:(NSString*)userId
                  userName:(NSString*)userName
                  userPass:(NSString*)userPass
                   success:(void (^)())success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
