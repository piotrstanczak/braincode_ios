//
//  AllegroApiRequestSerializer.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "AllegroApiRequestSerializer.h"

@implementation AllegroApiRequestSerializer

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request withParameters:(id)parameters error:(NSError *__autoreleasing *)error
{
    NSMutableDictionary *noNullparameters = [NSMutableDictionary new];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        if (obj != [NSNull null])
        {
            [noNullparameters setObject:obj forKey:key];
        }
    }];
    
    NSMutableURLRequest *resultRequest = [[super requestBySerializingRequest:request withParameters:noNullparameters error:error] mutableCopy];
    
    NSLog(@"REQUEST URL: %@", resultRequest.URL);
    //NSLog(@"HEADERS: %@", resultRequest.allHTTPHeaderFields);
    
    return resultRequest;
}

@end
