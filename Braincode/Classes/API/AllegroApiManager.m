//
//  AllegroApiManager.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "AllegroApiManager.h"
#import "AllegroApiRequestSerializer.h"
#import "JsonParser.h"
#import <CommonCrypto/CommonHMAC.h>
#import <NSHash/NSString+NSHash.h>

#define pathAllegroApi          @"https://api.natelefon.pl/"

#define keyLogin                @"userLogin"
#define keyPassword             @"hashPass"
#define pathLoginUser           @"/v1/allegro/login"
#define pathCategories          @"/v1/allegro/categories"
#define pathAuthentication      @"/v1/oauth/token?grant_type=client_credentials"
#define keyCategories           @"categories"
#define keyData                 @"data"
#define keyToken                @"access_token"
#define keyMessage              @"message"
#define keyStatus               @"error_code"
#define keyMenu                 @"menu"
#define keyError                @"error"

static AllegroApiManager *sharedManager = nil;

@interface AllegroApiManager ()

@property (nonatomic, strong) NSString *accessToken;

@end

@implementation AllegroApiManager

- (id)init
{
    NSAssert(sharedManager == nil, @"Can be only one instance of ApiManager class.");
    
    NSURL *url = [[NSURL alloc] initWithString:pathAllegroApi];
    self = [super initWithBaseURL:url];
    
    if (self)
    {
        self.requestSerializer = [AllegroApiRequestSerializer new];
        
        [self.requestSerializer setAuthorizationHeaderFieldWithUsername:@"braincode.mobi.2015" password:@"smAamczp"];
        
        // Nagłówki
//        [self.requestSerializer setValue:kApiKeyValue forHTTPHeaderField:kApiKey];
//        [self.requestSerializer setValue:LANGUAGE forHTTPHeaderField:kApiLanguageKey];
        
        self.responseSerializer = [AFJSONResponseSerializer new];
    }
    
    return self;
}

+ (instancetype)sharedManager
{
    @synchronized(self)
    {
        if (sharedManager == nil)
        {
            sharedManager = [[AllegroApiManager alloc] init];
        }
    }
    return sharedManager;
}

- (void)authenticationWithSuccess:(void (^)())success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [super GET:pathAuthentication parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        _accessToken = [JsonParser stringWithJson:responseObject[keyToken]];

        if (_accessToken && success)
            success();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation, error);
    }];
}

- (NSString*)toSHA256ForKey:(NSString*)key
{
    NSData *inputData = [key dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *outputData = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(inputData.bytes, (CC_LONG)inputData.length, outputData.mutableBytes);
    
    if ([outputData respondsToSelector:@selector(base64Encoding)]) {
        return [outputData base64Encoding];
    }
    
    return [outputData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
              accessToken:(NSString *)accessToken
                  success:(void (^)(NSString *userId))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *passSHA256 = [self toSHA256ForKey:password];
    
    NSDictionary *parameters = @{
                                  keyToken      : _accessToken,
                                  keyLogin      : username,
                                  keyPassword   : passSHA256
                                  };
    
    [self.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [super POST:pathLoginUser parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *userId = [JsonParser stringWithJson:responseObject[@"userId"]];
        NSLog(@"userId: %@", userId);
        
        //NSLog(@"!!!!REQUEST!!!! %@", operation.request.allHTTPHeaderFields);
        
        if (userId && success)
            success(userId);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation, error);
    }];
}

- (void)getCategoriesWithSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    NSDictionary *parameters = @{
                                 keyToken      : _accessToken
                                 };
    
    [self.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [super GET:pathCategories parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *dataArray = [JsonParser arrayWithJson:responseObject[keyCategories]];
        
        if (dataArray && success)
            success(dataArray);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure)
            failure(operation, error);
        
    }];
}


@end
