//
//  AllegroApiManager.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface AllegroApiManager : AFHTTPRequestOperationManager

+ (instancetype)sharedManager;

- (void)authenticationWithSuccess:(void (^)())success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)loginWithUsername:(NSString *)username
                 password:(NSString *)password
              accessToken:(NSString *)accessToken
                  success:(void (^)(NSString *userId))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getCategoriesWithSuccess:(void (^)(NSArray *data))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
