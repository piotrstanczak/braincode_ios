//
//  BaseViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuViewController.h"
#import "OffersViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _rightNavBarItems = [[NSArray alloc] init];
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.title = self.viewTitle;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"MenuIcon"] style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];
    
    /*UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"M" style:UIBarButtonItemStylePlain target:self.viewDeckController action:@selector(toggleLeftView)];*/
    
    self.navigationItem.leftBarButtonItem = menuBarButton;
}

#pragma mark - IBAction methods

- (IBAction)goToModuleWithMenuId:(NSString *)menuId
{
    NSLog(@"MenuId = %@", menuId);
    
    if (menuId)
    {
        MenuViewController *leftVC = ((MenuViewController *)self.viewDeckController.leftController);
        
        /*MenuItem *menuItem = [MenuItem getMenuItemForMenuId:menuId];
        
        [leftVC launchModuleWithMenuItem:menuItem];
        
        [leftVC selectRowForMenuId:menuId];*/
    }
}
@end
