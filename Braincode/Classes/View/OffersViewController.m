//
//  OffersViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "OffersViewController.h"
#import "ContentManager.h"
#import "OfferItem.h"
#import "OffelViewCell.h"
#import "OfferDetailViewController.h"

@interface OffersViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *offersData;
@property (nonatomic, assign) NSInteger currentOfferItemIndex;

@end

@implementation OffersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Oferty";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromRGB:MENU_COLOR]};

    
    [self setupTableView];
    [self getNewOffers];
}

- (void)setupTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowDetailedOffer"])
    {
        OfferDetailViewController *offerDetailViewController = (OfferDetailViewController*)[segue destinationViewController];
        
        OfferItem *item = [self.offersData objectAtIndex:_currentOfferItemIndex];
        
        [offerDetailViewController setupWithOffer:item];
    }
}

- (void)getNewOffers
{
    [[ContentManager sharedManager] getOffersWithSuccess:^(NSArray *data) {
        
        NSLog(@"OffersViewController: %@", @(data.count));
        
        self.offersData = data;
        
        [self.tableView reloadData];
        
    } failure:^(NSInteger errorCode, NSString *errorMessage) {
        
        NSLog(@"OffersViewController: %@ %@", @(errorCode), errorMessage);
        
    } view:self.view];
}

#pragma mark - TableViewDataSource delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.offersData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OfferCell";
    
    OffelViewCell *cell = (OffelViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [cell.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    OfferItem *item = [self.offersData objectAtIndex:indexPath.row];
    
    NSLog(@"item: %@", item.title);
    
    [cell initWithItem:item];
    // cell.textLabel.text = item.title;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _currentOfferItemIndex = indexPath.row;
    
    
    [self performSegueWithIdentifier:@"ShowDetailedOffer" sender:self];
}

@end
