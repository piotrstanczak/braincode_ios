//
//  OfferDetailViewController.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferItem.h"

@interface OfferDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (void)setupWithOffer:(OfferItem*)offerTime;

@end
