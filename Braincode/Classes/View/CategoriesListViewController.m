//
//  CategoriesListViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "CategoriesListViewController.h"
#import "ContentManager.h"
#import "CategoriesItem.h"

@interface CategoriesListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *categoriesData;
@end

@implementation CategoriesListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Kategorie";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromRGB:MENU_COLOR]};
    
    [self setupMenuView];
    [self getCategoriesData];
}

- (void)setupMenuView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)getCategoriesData
{
    [[ContentManager sharedManager] getCategoriesWithSuccess:^(NSArray *data) {
        
        self.categoriesData = data;
        
        [self.tableView reloadData];
        
        NSLog(@"CategoriesListViewController: %@", @(data.count));
        
    } failure:^(NSInteger errorCode, NSString *errorMessage) {
        
    } view:self.view];
}

#pragma mark - TableViewDataSource delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.categoriesData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [cell.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    CategoriesItem *item = [self.categoriesData objectAtIndex:indexPath.row];
    
    NSLog(@"item: %@", item.title);
    
    cell.backgroundColor = [UIColor colorFromRGB:MENU_COLOR];
    cell.textLabel.textColor = [UIColor whiteColor];

    cell.textLabel.text = item.title;

    return cell;
}

@end
