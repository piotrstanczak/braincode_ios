//
//  DeckViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "DeckViewController.h"
#import "MenuViewController.h"

#define VIEW_DECK_WIDTH								275.0

@interface DeckViewController ()

@end


@implementation DeckViewController


- (id)init
{
    UIStoryboard *leftMenuStoryboard = [UIStoryboard storyboardWithName:@"MenuStoryboard" bundle:nil];
    MenuViewController *leftMenuVC = [leftMenuStoryboard instantiateInitialViewController];
    
    UIStoryboard *offersStoryboard = [UIStoryboard storyboardWithName:@"OffersStoryboard" bundle:nil];
    
    UINavigationController *centerNewsNavigationController = [offersStoryboard instantiateInitialViewController];

    
    self = [super initWithCenterViewController:centerNewsNavigationController leftViewController:leftMenuVC];
    
    if (self)
    {
        self.delegate = self;
        self.elastic = NO;
        self.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.leftSize = self.view.width - VIEW_DECK_WIDTH;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



#pragma mark - IIViewDeckControllerDelegate methods

- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldOpenViewSide:(IIViewDeckSide)viewDeckSide
{
    //	if ([self.centerController.topViewController isKindOfClass:[CoreVC class]])
    //	{
    //		return [(CoreVC *)self.centerController.topViewController shouldOpenViewSide:viewDeckSide];
    //	}
    
    return YES;
}

@end