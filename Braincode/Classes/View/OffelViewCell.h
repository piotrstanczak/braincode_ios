//
//  OffelViewCell.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferItem.h"

@interface OffelViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)initWithItem:(OfferItem*)item;

@end
