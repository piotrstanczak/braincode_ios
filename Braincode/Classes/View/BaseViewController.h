//
//  BaseViewController.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) NSString *viewId;
@property (nonatomic, strong) NSString *viewTitle;

@property (nonatomic, strong) NSArray *rightNavBarItems;


- (IBAction)goToModuleWithMenuId:(NSString *)menuId;


@end
