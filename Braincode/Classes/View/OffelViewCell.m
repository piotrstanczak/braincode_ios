//
//  OffelViewCell.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "OffelViewCell.h"
#import "OfferItem.h"

@implementation OffelViewCell

- (void)initWithItem:(OfferItem*)item
{
    self.titleLabel.text = item.title;
    
    UIImage *image = [UIImage imageWithData:item.imageData];
    self.image.image = image;
    
    /*__weak OffelViewCell *weakCell = self;
    [self.image setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:item.imageUrl]] placeholderImage:nil success: ^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        weakCell.image.image = image;
        [weakCell setNeedsLayout];
        
    } failure:nil];*/
}


@end
