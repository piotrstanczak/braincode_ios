//
//  BookedViewController.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "BaseViewController.h"

@interface BookedViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
