//
//  DeckViewController.h
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "IIViewDeckController.h"

@interface DeckViewController : IIViewDeckController  <IIViewDeckControllerDelegate>

@property (nonatomic, strong) UINavigationController *centerController;

@end
