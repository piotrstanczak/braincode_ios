//
//  OfferDetailViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "OfferDetailViewController.h"
#import "OfferItem.h"

@interface OfferDetailViewController ()

@property (nonatomic, strong) OfferItem *offerItem;

@end

@implementation OfferDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Szczegóły oferty";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromRGB:MENU_COLOR]};
    
    // [self getNewOffer];
}

- (void)setupWithOffer:(OfferItem*)offerTime
{
    NSLog(@"setupWithOffer");
    self.offerItem = offerTime;
    
    UIImage *image = [UIImage imageWithData:self.offerItem.imageData];
    self.imageView.image = image;
    
    [self updateViewConstraints];
}

@end
