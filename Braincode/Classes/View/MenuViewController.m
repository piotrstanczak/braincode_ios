//
//  MenuViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "MenuViewController.h"
#import "BaseViewController.h"
#import "UINavigationController+FMA.h"
#import "MenuItem.h"

@interface MenuViewController ()

@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, assign) NSInteger currentMenuItem;

@end

@implementation MenuViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self configureMenu];
    [self setupMenuView];
}

- (void)configureMenu
{
    _currentMenuItem = 0;
    
    MenuItem *menuItem = [[MenuItem alloc] initWithMenuId:@"Offers" andTitle:@"Główna"];
    menuItem.isSelected = YES;
    
    self.menuItems = @[
                       menuItem,
                       [[MenuItem alloc] initWithMenuId:@"Categories" andTitle:@"Kategorie"],
                       [[MenuItem alloc] initWithMenuId:@"Booked" andTitle:@"Zabukowane"],
                       [[MenuItem alloc] initWithMenuId:@"Add" andTitle:@"Wystaw"],
                       [[MenuItem alloc] initWithMenuId:@"Settings" andTitle:@"Ustawienia"],
                       [[MenuItem alloc] initWithMenuId:@"Login" andTitle:@"Zaloguj"]
                       ];
}

- (void)setupMenuView
{
    self.menuTableView.backgroundColor = [UIColor colorFromRGB:0x90278e];
    self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    
    [self.menuTableView reloadData];
}

#pragma mark - TableViewDataSource delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    MenuItem *menuItem = [self.menuItems objectAtIndex:indexPath.row];
    
    if (_currentMenuItem == indexPath.row)
    {
        cell.backgroundColor = [UIColor colorFromRGB:MENU_SELECTED_COLOR];
    }
    else
    {
        cell.backgroundColor = [UIColor colorFromRGB:MENU_COLOR];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGSize itemSize = CGSizeMake(40, 40);
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [cell.imageView.image drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"MenuIcon_%d", (int)indexPath.row]];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = menuItem.title;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [self.menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_currentMenuItem inSection:0]];
    cell.backgroundColor = [UIColor colorFromRGB:MENU_COLOR];
    MenuItem *menuItem = [self.menuItems objectAtIndex:indexPath.row];
    menuItem.isSelected = NO;
    _currentMenuItem = indexPath.row;
    
    cell = [self.menuTableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorFromRGB:MENU_SELECTED_COLOR];
    
    menuItem = [self.menuItems objectAtIndex:indexPath.row];
    menuItem.isSelected = YES;
    
    if (menuItem)
    {
        [self launchModuleWithMenuItem:menuItem];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Brak zaimplementowanego widoku", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)launchModuleWithMenuItem:(MenuItem *)menuItem
{
    // Budowanie po menuId, te widoki związane z WebView trzeba będzie zrobić różne, ale będą dziedziczyć po jakimś wspólnym VC
    // TODO: trzeba uporządkować te klasy dot. WebView
    NSString *storyboardName = [NSString stringWithFormat:@"%@Storyboard", menuItem.menuId];
    NSLog(@"storyboardName = %@", storyboardName);
    
    // try instantiate from stroyboard
    if ([[NSBundle mainBundle] pathForResource:storyboardName ofType:@"storyboardc"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        UINavigationController *centerNavigationController = [storyboard instantiateInitialViewController];
        
        ((BaseViewController *)centerNavigationController.rootViewController).viewId = menuItem.menuId;
        ((BaseViewController *)centerNavigationController.rootViewController).viewTitle = menuItem.title;
        
        [self.viewDeckController setCenterController:centerNavigationController];
        [self.viewDeckController closeLeftViewAnimated:YES];
    }
//    else if ([menuItem.menuId containsSubstring:@"Logout"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Log out", nil) message:NSLocalizedString(@"Wylogowano pomyślnie", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//        [alertView show];
//    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Brak zaimplementowanego widoku", nil) message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
}

@end
