//
//  BookedViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/13/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "BookedViewController.h"
#import "ContentManager.h"
#import "OfferItem.h"
#import "OffelViewCell.h"

@interface BookedViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *offersData;

@end

@implementation BookedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Zabukowane";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromRGB:MENU_COLOR]};
    
    
    [self setupTableView];
    [self getNewOffers];
}

- (void)setupTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)getNewOffers
{
    [[ContentManager sharedManager] getOffersWithSuccess:^(NSArray *data) {
        
        NSLog(@"OffersViewController: %@", @(data.count));
        
        self.offersData = data;
        
        [self.tableView reloadData];
        
    } failure:^(NSInteger errorCode, NSString *errorMessage) {
        
        NSLog(@"OffersViewController: %@ %@", @(errorCode), errorMessage);
        
    } view:self.view];
}

#pragma mark - TableViewDataSource delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.offersData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OfferCell";
    
    OffelViewCell *cell = (OffelViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    [cell.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    OfferItem *item = [self.offersData objectAtIndex:indexPath.row];
    
    NSLog(@"item: %@", item.title);
    
    [cell initWithItem:item];
    // cell.textLabel.text = item.title;
    
    return cell;
}

@end
