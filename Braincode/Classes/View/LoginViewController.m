//
//  LoginViewController.m
//  Braincode
//
//  Created by Piotr Stanczak on 3/14/15.
//  Copyright (c) 2015 Piotr Stanczak. All rights reserved.
//

#import "LoginViewController.h"
#import "TextFieldValidator.h"
#import "UserManager.h"

#define REGEX_USER_NAME_LENGTH @"^.{3,100}$"
#define REGEX_PASSWORD_LENGTH @"^.{8,100}$"

#define RED_COLOR [UIColor colorFromRGB:MAIN_RED_COLOR]

@interface LoginViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet TextFieldValidator *loginLabel;
@property (strong, nonatomic) IBOutlet TextFieldValidator *passLabel;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Logowanie";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromRGB:MENU_COLOR]};
    
    self.loginLabel.delegate = self;
    self.passLabel.delegate = self;
    
    [self.loginLabel addRegx:REGEX_USER_NAME_LENGTH withMsg:@"Nazwa użytkownika powina mieć co najmniej 3 znaki"];
    self.loginLabel.presentInView = self.view;
    [self.passLabel addRegx:REGEX_PASSWORD_LENGTH withMsg:@"Hasło powinno zawierać co najmniej 8 znaków"];
    self.passLabel.presentInView = self.view;
}

#pragma mark - UITextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField endEditing:YES];
    if (textField == self.loginLabel) {
        [self.passLabel becomeFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [(TextFieldValidator *)textField underlineLayer].backgroundColor = RED_COLOR.CGColor;
    [self clearPopups];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [(TextFieldValidator *)textField underlineLayer].backgroundColor = [UIColor grayColor].CGColor;
    [self clearPopups];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)loginHandler:(id)sender
{
    [self loginUser];
    //[self goToModuleWithMenuId:@"News"];
}

- (void)loginUser
{
//    if (![self validateForm]) {
//        return;
//    }
    
    // USER DATA FOR DEBUGING
    
    // user ok
    self.loginLabel.text = @"pssssss";
    self.passLabel.text = @"Piotr_123";
    
    // user nieaktywowany
    //self.userNameTextField.text = @"gfdgsf";
    //self.passwordTextField.text =  @"DASdsnfasjk123gfdgfd";
    
    // user niepoprawne
    //self.userNameTextField.text = @"bla";
    //self.passwordTextField.text =  @"bla";
    
    //self.userNameTextField.text = @"Boss999";
    //self.passwordTextField.text = @"AAAA9999";
    
    // 144, 40, 141
    
    [[UserManager sharedManager] loginWithUsername:self.loginLabel.text password:self.passLabel.text success:^() {
        
        User *user = [[UserManager sharedManager] user];
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Logowanie" message:[NSString stringWithFormat:@"Zalogowano użytkownika %@", user.username]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        
    } failure:^(NSInteger errorCode, NSString *errorMessage) {
        
        NSLog(@"Login failed %@ %@", @(errorCode), errorMessage);
        
    } view:self.view];
}

- (void)clearPopups
{
    [self.loginLabel dismissPopup];
    [self.passLabel dismissPopup];
}

- (BOOL)validateForm
{
    return ([self.loginLabel validate] & [self.passLabel validate]) ? YES : NO;
}

@end
